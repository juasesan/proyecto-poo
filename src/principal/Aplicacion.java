package principal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import usuarios.*;
import Transaccionables.Solicitud;

public class Aplicacion {
    private Administrador administrador;
    private ArrayList<Usuario> usuarios;
    Scanner sc = new Scanner(System.in);
    
    public Aplicacion(){
        administrador = new Administrador("Alonzo", "Gonzalez", "alogonzal", "Trati795cuquer", 'A');
        usuarios = new ArrayList<>();
    }
    
    public void ingresarUsuario(Usuario s){
        usuarios.add(s);
    }
    
    public static void main(String[] args) throws IOException{
        boolean bandera = true;
        Aplicacion app = new Aplicacion();
        
        while(bandera){
            Scanner sc = new Scanner(System.in);
            System.out.println("\n----------------------");
            System.out.println("    MENU PRINCIPAL    ");
            System.out.println("----------------------");
            System.out.println("\nUsuario: ");
            String usuario= sc.nextLine();
            System.out.println("\nContraseña: ");
            String contrasena= sc.nextLine();
            
            //leyendo el archivo en busca del usuario
            ArrayList<String> texto = ManejoArchivos.leer("usuarios.txt");
            String[] line = new String[4];           
            int posicion = 0;   //variable que guarda la posición del usuario en el ArrayList
            boolean validado = false;
            
            for(String linea: texto){ 
                String[] datos = linea.split(",");
                boolean datosCorrectos = usuario.equals(datos[2]) && contrasena.equals(datos[3]);             
                if(datosCorrectos){    //Verificando que los datos esten en el archivo
                    
                    validado = true;
                    line = datos;
                }
            }

            //Si los datos son correctos, se muestra el menu dependiendo del perfil del usuario
            if(validado){
                for(int l = 0; l < app.usuarios.size(); l++){
                if(app.usuarios.get(l).getUsuario().equals(usuario))
                    posicion = l;
                }
                switch(line[4]){
                    case "A":
                        Administrador admin = app.administrador;
                        app.menuAdministrador(admin);
                        //Menu del administrador
                        break;
                    
                    case "E":
                        //Menu de empleados
                        
                        app.menuEmpleado();
                        break;
                    
                    case "C":
                        Cliente c = (Cliente) app.usuarios.get(posicion);
                        app.menuCliente(c);
                        //Menu de clientes
                        break;
                }
            }
            else
                System.out.println("\nUsuario y/o contraseña incorrectos. Vuelva a intentar");
        }        
    }
            
    public void menuAdministrador(Administrador admin){
        int opcion = 0;
        while(opcion!=2){
            System.out.println("\n++++++++++++++++++++++++++++++++++");
            System.out.println("     BIENVENIDO ADMINISTRADOR     ");
            System.out.println("++++++++++++++++++++++++++++++++++");
            System.out.println("1 Crear un Usuario");
            System.out.println("2 Salir");
            System.out.println("Ingrese una Opcion:");
            opcion =sc.nextInt();
            if(opcion == 1){
                Usuario creado = admin.crearUsuario();
                if(creado != null){
                    this.ingresarUsuario(creado);
                }
            }
            else if(opcion == 2)
                System.out.println("Volviendo al menu principal");
            else
                System.out.println("Seleccione una opcion valida");
        }               
    }
    
    public void menuCliente(Cliente cliente){
        int opcion = 0;
        while(opcion!=6){
            System.out.println("\n++++++++++++++++++++++++++++++++++");
            System.out.println("           Menu Cliente           ");
            System.out.println("++++++++++++++++++++++++++++++++++");
            System.out.println("1.Abrir cuenta bancaria");
            System.out.println("2. Realizar transferencia");
            System.out.println("3. Solicitar Tarjeta");
            System.out.println("4 Consultar estado de solicitudes");
            System.out.println("5.Desactivar");
            System.out.println("6.Salir");
            System.out.println("\nIngrese una Opcion:");
            opcion = sc.nextInt();    
            switch (opcion){          
                case 1:
                    cliente.abrirCuenta();
                    break;
                case 2:
                    cliente.realizarTransferencia();
                    break;

                case 3:
                    cliente.solicitarTarjeta();
                    break;
                case 4:
                    cliente.consultarSolicitudes();
                    break;
                
                case 5:
                    System.out.println("1. Cuenta \n2. Solicitud \n3. Tarjeta \nEscoja objeto a desactivar:");
                    int option = sc.nextInt();
                    switch(option){
                        case 1:
                            if(cliente.getCuenta() == null)
                                System.out.println("No existe cuenta asociada a este usuario. Volviendo al menu de cliente");
                            else
                                cliente.desactivar(cliente.getCuenta());
                            break;
                            
                        case 2:
                            if(cliente.getSolicitudes().isEmpty())
                                System.out.println("Usted no ha realizado solicitudes aun");
                            else{
                                ArrayList<Solicitud> solicitudes = cliente.getSolicitudes();
                                System.out.println("Sus solicitudes son:");
                                for(int i = 0; i < solicitudes.size(); i++){
                                    System.out.println((i+1) + ". " + cliente.getSolicitudes().get(i).getID());
                                }
                                System.out.println(solicitudes.size()+1 + ". Regresar");
                                System.out.println("Seleccione solicitud a desactivar:");
                                int op = sc.nextInt();
                                while(op != solicitudes.size()+1){
                                    if(op >= 1 && op <= solicitudes.size())
                                        cliente.desactivar(solicitudes.get(op-1));
                                    else if(op == solicitudes.size()+1)
                                        System.out.println("Volviendo al menu de cliente");
                                    else
                                        System.out.println("Ingrese una opcion valida");
                                }                               
                            }
                            break;
                            
                        case 3:
                            if(cliente.getTarjetas().isEmpty())
                                System.out.println("No existen tarjetas asociadas a este usuario. Volviendo al men de cliente");
                    }
                    break;
                    
                case 6:
                    System.out.println("Volviendo al menu principal");
                    break;
                    
                default:
                    System.out.println("Seleccione una opcion valida");
            }
            

        }
    }
    
    public void menuEmpleado(){
        int opcion = 0;
        while(opcion!=2){
            System.out.println("\n++++++++++++++++++++++++++++++++++");
            System.out.println("           MENU EMPLEADO          ");
            System.out.println("++++++++++++++++++++++++++++++++++");
            System.out.println("1.Aprobar creacion de cuentas");
            System.out.println("2.Aprobar solicitudes de tarjetas");
            System.out.println("3.Obtener reporte de clientes");
            System.out.println("4.Salir");
            System.out.println("\nIngrese una Opcion");
            int op2=sc.nextInt();
        }
    }
}
    

