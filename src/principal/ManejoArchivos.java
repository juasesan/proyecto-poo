package principal;
import java.io.*;
import java.util.ArrayList;

public class ManejoArchivos 
{
    public static ArrayList<String> leer(String nombreArchivo)
    {
        ArrayList<String> texto = new ArrayList<>();
        try{
            FileReader fr = new FileReader(nombreArchivo);
            BufferedReader br = new BufferedReader(fr);
            String linea;
            while((linea = br.readLine())!=null){
                texto.add(linea);
            }            
            return texto;
        }
        catch(Exception err){
            System.out.println("Archivo no encontrado");
            return null;
        }       
    }
    
    public static void escribir(String nombreArchivo,String mensaje){
        File f = new File(nombreArchivo);
        if(f.exists()){
            try{
                FileWriter fw = new FileWriter(nombreArchivo,true);
                BufferedWriter bw = new BufferedWriter(fw);

                bw.write(mensaje);
                bw.newLine();

                bw.close();

            }
            catch(IOException e){
                System.out.println("No se pudo escribir en el archivo");
            }      
        }   
    }
}

