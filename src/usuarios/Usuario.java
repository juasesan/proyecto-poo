package usuarios;

public class Usuario {
    private String nombre;
    private String apellido;
    private String usuario;
    private String contrasena;
    private char perfil;
    
    public Usuario(String n, String a, String u, String c, char p){
        nombre = n;
        apellido = a;
        usuario = u;
        contrasena = c;
        perfil = p;
    }
    
    public String getNombre(){
        return this.nombre;
    }
    
    public String getApellido(){
        return this.apellido;
    }
    
    public String getUsuario(){
        return this.usuario;
    }
    
    public String getContrasena(){
        return this.contrasena;
    }
    
    public char getPerfil(){
        return this.perfil;
    }
}
