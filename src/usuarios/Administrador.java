package usuarios;

import java.util.Scanner;
import principal.ManejoArchivos;

public class Administrador extends Usuario{
    public Administrador(String n, String a, String u, String c, char p){
        super(n, a, u, c, p);
    }

    public Usuario crearUsuario(){
        int opcion = 0;
        while(opcion != 3){                    
            System.out.println("\n1. EMPLEADO");
            System.out.println("2. CLIENTE");
            System.out.println("3. Salir");
            Scanner sc = new Scanner(System.in);           
            System.out.println("¿Que perfil tendra el nuevo usuario?");
            opcion = sc.nextInt();
            switch (opcion) {
                case 1:
                    System.out.println("Has seleccionado la opcion 1");
                    Scanner entrada = new Scanner(System.in);                       
                    System.out.print("\nIngrese el nombre: ");
                    String nombre = entrada.nextLine();
                    System.out.print("\nIngrese el apellido: ");
                    String apellido = entrada.nextLine();
                    System.out.print("\nIngrese el usuario: ");
                    String usuario = entrada.nextLine();
                    String contra;

                    boolean correctPass = false;

                    while(correctPass == false){
                        System.out.println("\nLa contraseña a ingresar debe tener las siguientes caracteristicas:"
                            + "\n*Iniciar con una letra mayuscula."
                            + "\n*Tener una longitud de 8 a 12 caracteres");
                        System.out.print("\nIngrese la contraseña: ");
                        contra = entrada.nextLine();
                        char[] caracter = contra.toCharArray();
                        char ma=caracter[0];

                        if(Character.isUpperCase(ma) && contra.length() > 7 && contra.length()< 13){                          
                            String linea= String.join(",", nombre, apellido, usuario, contra, "E");    //Linea que se escribirá en el documento
                            ManejoArchivos.escribir("usuarios.txt", linea);
                            Usuario empleado = new Empleado(nombre, apellido, usuario, contra, 'E');
                            System.out.println("Se registro el usuario con exito");
                            correctPass = true;
                            return empleado;
                        }
                        
                        else{
                            System.out.println("Su contrtasena es incorrecta, intente de nuevo");
                        }                        
                    }                                             
                    break;

                case 2:
                    System.out.println("Has seleccionado la opcion 2");
                    Scanner entrada1 = new Scanner(System.in);                        
                    String contra1;
                    System.out.print("\nIngrese el nombre: ");
                    String nombre1 = entrada1.nextLine();
                    System.out.print("\nIngrese el apellido: ");
                    String apellido1 = entrada1.nextLine();
                    System.out.print("\nIngrese el usuario: ");
                    String usuario1 = entrada1.nextLine();


                    boolean contraValida = false;

                    while(contraValida == false){
                        System.out.println("\nLa contrasena ingresada debe tener las siguientes caracteristicas:"
                            + "\n*Iniciar con una letra mayuscula."
                            + "\n*Tener una longitud de 8 a 12 caracteres");
                        System.out.print("\nIngrese la contrasena: ");
                        contra1 = entrada1.nextLine();
                        char[] caracter1 = contra1.toCharArray();
                        char ma1=caracter1[0];

                        if(Character.isUpperCase(ma1) && contra1.length() > 7 && contra1.length()< 13){                           
                            System.out.println("\nIngrese lugar de trabajo: ");
                            Scanner sa = new Scanner(System.in);
                            String lugarTrabajo = sa.nextLine();
                            String linea = String.join(",", nombre1, apellido1, usuario1, contra1, "C");  //Linea que se escribirá en el documento
                            ManejoArchivos.escribir("usuarios.txt", linea);
                            Usuario cliente = new Cliente(nombre1, apellido1, usuario1, contra1, 'C', lugarTrabajo);
                            System.out.println("\nSe registro el usuario con exito");
                            contraValida = true;
                            return cliente;
                        }
                        
                        else{
                            System.out.println("\nSu contrasena es incorrecta, intente de nuevo");
                        }
                        
                    }                                                
                    break;

                case 3:                    
                    break;
                    
                default:
                    System.out.println("\nSolo numeros entre 1 y 3");
            }
        }
        System.out.println("Operacion cancelada. Volviendo al menu principal");
        return null;
    }
}    