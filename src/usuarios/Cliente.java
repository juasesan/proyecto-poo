package usuarios;

import Transaccionables.*;
import java.util.ArrayList;
import java.util.Scanner;
import Transferencia.*;

public class Cliente extends Usuario {
    private Cuenta cuenta;      
    private String direccion;
    private double deudas;
    private double sueldo;
    private ArrayList<Solicitud> solicitudes;  
    private ArrayList<Tarjeta> tarjetas;
    
    public Cliente(String n, String a, String u, String c, char p, String direccion){
        super(n, a, u, c, p);
        this.direccion = direccion;
        deudas = 0;
        sueldo = 2000;
        solicitudes = new ArrayList<>();
        tarjetas = new ArrayList<>();
        cuenta = null;
    }
    
    public String getDireccion(){
        return direccion;
    }
       
    public double getDeudas(){
        return deudas;
    }
    
    public void setDeudas(double d){
        deudas = d;
    }
    
    public Cuenta getCuenta(){
        return cuenta;
    }
    
    public ArrayList<Solicitud> getSolicitudes() {
        return solicitudes;
    }
    
    public ArrayList<Tarjeta> getTarjetas() {
        return tarjetas;
    }
    
    public void agregarSolicitud(Solicitud s){
        solicitudes.add(s);
    }
    
    public void agregarTarjeta(Tarjeta t){
        tarjetas.add(t);
    }
    
    public void abrirCuenta(){
        char opcion = 'S';
        while(opcion == 'S'){
            Scanner sb = new Scanner(System.in);
            Scanner sa = new Scanner(System.in);
            //System.out.println("\nIngrese su usuario:");
            //String usuario = sb.nextLine();
            //if(usuario.equals(this.getUsuario())){  //Validando que el usuario ingresado sea correto
            System.out.println("\nIngrese el tipo de cuenta (1. Corriente, 2. Ahorros):");
            int tipo = sa.nextInt();

            if(tipo == 1 || tipo == 2){ //Validando que la opcion ingresada sea correcta                      
                System.out.println("\n¿Confirma la creacion de su cuenta? (S/N)");
                opcion = sb.next().charAt(0);
                switch(opcion){
                    case 'S':
                        //Generando numero de cuenta
                        String numeroCuenta = "";
                        for(int i = 0; i < 9; i++){                            
                            int entero = (int) Math.floor(Math.random()*9);
                            numeroCuenta += entero;
                        }

                        //Validando que el numero no este repetido
                        /*boolean noValidado = true;
                        while(noValidado){
                            ArrayList<String> cuentas = ManejoArchivos.leer("cuentas.txt");

                            for(String linea: cuentas){
                                boolean repetida = linea.contains(numeroCuenta);

                                if(repetida){
                                    numeroCuenta = "";  //Se formatea el numero de cuenta si esta se halla repetida
                                    for(int i = 0; i < 9; i++){   //Se crea un numero nuevo                         
                                        int entero = (int) Math.floor(Math.random()*9);
                                        numeroCuenta += entero;
                                    }
                                }
                            }

                        }*/
                        //Creando cuenta segun el tipo
                        switch(tipo){
                            case 1:
                                this.cuenta = new CuentaCorriente(this.getUsuario(), numeroCuenta);
                                CuentaCorriente corriente = (CuentaCorriente) cuenta;
                                corriente.guardarDatos();
                                System.out.println("\nCuenta corriente creada con exito.");
                                opcion = 'N';
                                break;

                            case 2:
                                Scanner sc = new Scanner(System.in);
                                System.out.println("\nIngrese tipo de plan:");
                                String plan = sc.nextLine();

                                this.cuenta = new CuentaAhorros(this.getUsuario(), numeroCuenta, plan);
                                CuentaAhorros ahorros = (CuentaAhorros) cuenta;
                                ahorros.guardarDatos();
                                System.out.println("\nCuenta de ahorros creada con exito.");
                                opcion = 'N';
                                break;                       
                        }
                        break;

                    case 'N':
                        System.out.println("\nOperacion cancelada. Volviendo al menu principal.");
                }

            }
            else
                System.out.println("\nOpcion no valida. Seleccione 1 o 2.");
            /*}
            else
                System.out.println("\nUsuario incorrecto. Vuelva a intentar");*/
        }
    }
    
    public void realizarTransferencia(){
        if(cuenta.getEstado().equals("Activa")){
            int opcion = 0;
            while(opcion != 3){
                Scanner sc = new Scanner(System.in);    //Para la opcion de transferencia
                Scanner sa = new Scanner(System.in);    //Para los datos de la transferencia
                System.out.println("Seleccione el tipo de transferencia:\n1. Directa \n2. Interbancaria \n3. Regresar al menu");
                opcion = sc.nextInt();
                
                if(opcion == 1 || opcion == 2 || opcion == 3){
                    switch(opcion){
                        case 1:
                            System.out.println("\n-----Transferencia Directa-----");
                            System.out.println("Ingrese numero de cuenta:");
                            String cuenta = sa.nextLine();
                            System.out.println("Ingrese monto:");
                            Double monto = sa.nextDouble();
                            double saldoDisponible = this.cuenta.getSaldo();
                            
                            if(monto <= saldoDisponible){
                                Transferencia transferencia = new Transferencia(cuenta, monto);
                                transferencia.guardarDatos();
                                this.cuenta.retirar(monto);
                                System.out.println("\nTransferencia exitosa!");
                                opcion = 3;
                            }
                            else{
                                System.out.println("Saldo en cuenta insuficiente.\nDeposite dinero en su cuenta para poder transferir.");
                                opcion = 3;
                            }                            
                            break;
                        
                        case 2:
                            Scanner sb = new Scanner(System.in);
                            System.out.println("\n-----Transferencia Interbancaria-----");
                            System.out.println("Ingrese los siguientes datos del beneficiario:");
                            System.out.println("\nNumero de cuenta:");
                            String numCuenta = sa.nextLine();
                            System.out.println("Banco:");
                            String banco = sa.nextLine();
                            System.out.println("Titular:");
                            String titular = sa.nextLine();
                            System.out.println("Cedula:");
                            String cedula = sa.nextLine();
                            System.out.println("Monto:");
                            Double monto2 = sb.nextDouble();
                            System.out.println("Descripcion:");
                            String descripcion = sa.nextLine();
                            double disponible = this.cuenta.getSaldo();
                            
                            if(monto2 <= disponible){
                                Transferencia t = new Interbancaria(numCuenta,monto2,banco,titular,cedula,descripcion);
                                t.guardarDatos();
                                this.cuenta.retirar(monto2);
                                System.out.println("\nTransferencia exitosa!");
                                opcion = 3;
                            }
                            else{
                                System.out.println("Saldo en cuenta insuficiente.\nDeposite dinero en su cuenta para poder transferir.");
                                opcion = 3;
                            }                           
                            break;
                            
                        case 3:
                            System.out.println("Volviendo al menu de cliente");
                            break;
                    }
                }
                else
                    System.out.println("Opcion no valida. Seleccione 1, 2 o 3.");
            }
        }
        else
            System.out.println("\nCuenta en estado inactivo.\nActive su cuenta para poder realizar transferencias.");
    }
    
    public void solicitarTarjeta(){
        if(cuenta.getEstado().equals("Activa")){
            int tipo = 0;
            while(tipo != 3){
                System.out.println("\n-----Solicitud de Tarjeta-----");
                System.out.println("1. Credito \n2. Debito \n3. Regresar \nSeleccione una opcion:");
                Scanner type = new Scanner(System.in);
                Scanner datos = new Scanner(System.in);
                tipo = type.nextInt();
           
                if(tipo == 1 || tipo == 2 || tipo ==3){
                    
                    //Comprobando que el usuario no haya solicitado tarjetas aún
                    boolean creditoExistente = false;
                    boolean debitoExistente = false;                    
                    for(Tarjeta t: tarjetas){
                        if(t instanceof TarjetaCredito)
                            creditoExistente = true;
                        else if(t instanceof Tarjeta)
                            debitoExistente = true;
                    }
                    
                    switch(tipo){
                        case 1:                              
                            if(creditoExistente == false){
                                Solicitud solicitud = new Solicitud(this.getUsuario(), "credito", this.getCuenta().getNumero(), this.getDireccion(), ""+sueldo, ""+tarjetas.size(), ""+deudas);
                                solicitud.guardarDatos();
                                this.solicitudes.add(solicitud);
                                tarjetas.add(new TarjetaCredito(this.getCuenta().getNumero(), 2000, 1000, "31/06/2022", "falso"));
                                System.out.println("Tarjeta creada. A la espera de activacion");
                                tipo = 3; 
                            }
                            else
                                System.out.println("Usted ya posee una tarjeta de credito. Operacion cancelada");
                                tipo = 3;
                            break;

                        case 2:
                            if(debitoExistente == false){
                                Solicitud solicitud2 = new Solicitud(this.getUsuario(), this.getCuenta().getNumero());
                                solicitud2.guardarDatos();
                                this.solicitudes.add(solicitud2);
                                tarjetas.add(new Tarjeta(this.getCuenta().getNumero()));
                                System.out.println("Tarjeta creada. A la espera de activacion");
                                tipo = 3;
                            }
                            else
                                System.out.println("Usted ya posee una tarjeta de debito. Operacion cancelada");
                                tipo = 3;
                            break;

                        case 3:
                            System.out.println("Volviendo al menu de cliente");
                            tipo = 3;
                            break;
                    }
                }

                else
                    System.out.println("Ingrese una opcion valida");
            }
        }
        
        else
           System.out.println("\nCuenta en estado inactivo.\nActive su cuenta para poder solicitar tarjetas.");
    }
    
    public void consultarSolicitudes(){
        for(Solicitud s: solicitudes){
            s.consultarEstado();
        }
    }
    
    public void desactivar(Transaccionable objeto){
        if(objeto instanceof Cuenta){
            Cuenta c = (Cuenta) objeto;
            c.setEstado("Desactivado");
            System.out.println("Estimado " + this.getNombre() + ", su cuenta ha sido desactivada. Para volver a activarla, comuníquese con su asesor bancario");
        }
        else if(objeto instanceof Solicitud){
            Solicitud s = (Solicitud) objeto;
            s.setEstado("Desactivado");
            System.out.println("Estimado " + this.getNombre() + ", esta solicitud ya no procede. Para poder solicitar una tarjeta debe generar una nueva solicitud.");
        }
        else if(objeto instanceof Tarjeta){
            Tarjeta t = (Tarjeta) objeto;
            for(int i = 0; i < 2; i++){
                if(tarjetas.get(i).equals(t)){
                    tarjetas.remove(i);
                    System.out.println("Estimado " + this.getNombre() + ", ya no podrá volver a utilizar su tarjeta de crédito número " + t.getNumero() + ".Si la necesita debe solicitar una nueva.");
                }
            }
        }
    }
}