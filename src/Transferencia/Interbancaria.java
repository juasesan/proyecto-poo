package Transferencia;

import java.util.Random;
import principal.ManejoArchivos;

/**
 *
 * @author alfre
 */
public class Interbancaria extends Transferencia {
    
    private String sep =",";
    private String banco;
    private String titular;
    private String cedula;
    private String descripcion;
    
    public Interbancaria(String numeroDeCuenta, double monto, String banco, String titular, String cedula, String descripcion){
        super(numeroDeCuenta,monto);
        this.banco = banco;
        this.titular = titular;
        this.cedula = cedula;
        this.descripcion = descripcion;       
    }
    
    @Override
    public void guardarDatos(){      
        String montoS= Double.toString(this.getMonto());
        String linea = String.join(",", this.getID(), "Interbancaria", this.getNumeroDeCuenta(), montoS, banco, titular, cedula, descripcion);
        ManejoArchivos.escribir("transferencias.txt", linea);
    
    }
}
