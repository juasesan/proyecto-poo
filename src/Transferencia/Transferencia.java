package Transferencia;
import java.util.Random;
import principal.ManejoArchivos;

public class Transferencia {  
    private String id;
    private String numeroDeCuenta;
    private double monto;
    
    public Transferencia(String numeroDeCuenta, double monto){
        this.numeroDeCuenta=numeroDeCuenta;
        this.monto=monto;
        id = this.generarID();
    }

    private String generarID(){
        String id = "";
        for(int i = 0; i < 4; i++){
            int entero = (int) Math.floor(Math.random()*9);
            id += entero;
        }
        return id;       
    }
    
    public String getNumeroDeCuenta() {
        return numeroDeCuenta;
    }

    public double getMonto() {
        return monto;
    }
    
    public String getID(){
        return id;
    }

    public void setNumeroDeCuenta(String numeroDeCuenta) {
        this.numeroDeCuenta = numeroDeCuenta;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }
    
    public void guardarDatos(){
        String linea = String.join(",", id, "Directa", numeroDeCuenta, Double.toString(monto), "---", "---", "---", "---");
        ManejoArchivos.escribir("transferencias.txt", linea);
    }
}