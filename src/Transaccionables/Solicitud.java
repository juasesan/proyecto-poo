package Transaccionables;
import java.text.SimpleDateFormat;
import principal.ManejoArchivos;
import java.util.Date;

public class Solicitud implements Transaccionable{
    private String usuario;
    private String numeroCuenta;
    private String direccion;
    private String sueldo;
    private String cantidadTarjetas;
    private String valorDeudas;
    private String tipo;
    private String estado ;
    private String id;

    public Solicitud(String usuario, String numeroCuenta){    //Para solicitud de Débito                            
        this.usuario = usuario;
        this.numeroCuenta = numeroCuenta;
        direccion = "---";
        sueldo = "---";
        cantidadTarjetas = "---";
        valorDeudas = "---";
        tipo = "debito";
        this.generarid();
        estado = "Pendiente";
    }
    
    //Para solicitud de Credito
    public Solicitud(String usuario, String tipo, String numeroCuenta, String direccion, String sueldo, String cantidadTarjetas, String valorDeudas){ 
        this.usuario = usuario;
        this.numeroCuenta = numeroCuenta;
        this.direccion = direccion;
        this.sueldo = sueldo;
        this.cantidadTarjetas = cantidadTarjetas;
        this.valorDeudas = valorDeudas;
        this.tipo = tipo;
        this.generarid();
        estado = "Pendiente";
    }
    
    private void generarid(){
        String id = "";
        for(int i = 0; i < 4; i++){                            
            int entero = (int) Math.floor(Math.random()*9);
            id += entero;
        }
        this.id = id;
    }
    
    public String getID(){
        return this.id;
    }
    
    public String getEstado(){
        return estado;
    }
    
    public void setEstado(String e){
        estado = e;
    }
    
    @Override
    public void consultarEstado() {         
        Date date = new Date();
        String formato = "dd/MM/yyyy";  //formato de fecha deseado
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formato);
        String fecha = simpleDateFormat.format(date);
        
        switch(tipo){
            case "cuenta":
                System.out.println("Su solicitud de  creacion de cuenta con número: "+ numeroCuenta + " tiene estado\n" +
                    estado + " a la fecha: "+ fecha);
                break;
                
            default:
                System.out.println("Su solicitud de  tarjeta de " +tipo+ " con número: "+ numeroCuenta + " tiene estado\n" +
                    estado + " a la fecha: "+ fecha); 
                break;
        }              
    }
    
    public void guardarDatos(){
        String linea = String.join(",", id, usuario, tipo, numeroCuenta, direccion, sueldo, cantidadTarjetas, valorDeudas, estado);
        ManejoArchivos.escribir("solicitudes.txt", linea);
    }
}