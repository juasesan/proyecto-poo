package Transaccionables;

import java.text.SimpleDateFormat;
import principal.ManejoArchivos;
import java.util.Date;

public class CuentaAhorros extends Cuenta{
    private String plan;

    public CuentaAhorros(String usuario, String numero, String plan){
        super(usuario, numero);
        this.plan = plan;
    }

    @Override
    public void guardarDatos(){
        Date date = new Date();
        String formato = "dd/MM/yyyy";  //formato de fecha deseado
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formato);
        String fecha = simpleDateFormat.format(date);
        String linea= String.join(",", this.getID(), this.getUsuario(), "Ahorros", this.getNumero(), this.getEstado(), "---", plan, fecha.toString());              
        ManejoArchivos.escribir("cuentas.txt", linea);
    }
    
    @Override
    public void consultarEstado() {         
        Date date = new Date();
        String formato = "dd/MM/yyyy";  //formato de fecha deseado
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formato);
        String fechaActual = simpleDateFormat.format(date);
        System.out.println("Su cuenta de ahorros con número:"+ this.getNumero() +" tiene estado\n" +
                    this.getEstado() + " a la fecha: "+ fechaActual);        
    }
}
