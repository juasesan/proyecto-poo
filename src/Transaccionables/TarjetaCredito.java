package Transaccionables;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TarjetaCredito extends Tarjeta{
    private double montoAprobado;
    private double SaldoActual;
    private String fechaCorte;
    private String enMora;
    
    public TarjetaCredito(String numero) {
        super(numero);
    }

    public TarjetaCredito(String numero, double montoAprobado, double SaldoActual, String fechaCorte, String enMora) {
        super(numero);
        this.montoAprobado = montoAprobado;
        this.SaldoActual = SaldoActual;
        this.fechaCorte = fechaCorte;
    }
       
    @Override
    public void consultarEstado() {        
        Date date = new Date();
        String formato = "dd/MM/yyyy";  //formato de fecha deseado
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formato);
        String fecha = simpleDateFormat.format(date);
        
        System.out.println("Su tarjeta de debito con numero: " + this.getNumero() + " tiene estado " + this.getEstado() + " a la fecha " + fecha);
        
        /*try {
            Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(fechaCorte);
            Date actual = new Date();
            if (date1.compareTo(actual)>0){
                System.out.println("Su tarjeta de crédito con número:"+ super.getNumero()+" tiene estado\n" +
                "activo a la fecha ."+ actual.toString());
            }
            else{
                System.out.println("Su tarjeta de crédito con número:"+ super.getNumero()+" tiene estado\n" +
                "vencido a la fecha ."+ actual.toString());
            }
        
        
        } catch (ParseException ex) {
            Logger.getLogger(TarjetaCredito.class.getName()).log(Level.SEVERE, null, ex);
        }  */      
    }   
}

