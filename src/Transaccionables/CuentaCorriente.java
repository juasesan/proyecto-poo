package Transaccionables;

import java.text.SimpleDateFormat;
import java.util.Date;
import principal.ManejoArchivos;

public class CuentaCorriente extends Cuenta{
    private Cheque[] chequera;
    private String idChequera;
    
    public CuentaCorriente(String usuario, String numero){
        super(usuario, numero);
        chequera = new Cheque[50];
        int codigo = this.generarid();
        idChequera = "" + codigo;   
        for(int i = 0; i < 50; i++){
            codigo += i;
            chequera[i] = new Cheque(codigo);
        }
    }
    
    private int generarid(){
        String id = "";
        for(int i = 0; i < 4; i++){                            
            int entero = (int) Math.floor(Math.random()*9);
            id += entero;
        }
        return Integer.parseInt(id);
    }
    
    public String getIdChequera(){
        return idChequera;
    }
    
    public Cheque[] getChequera(){
        return chequera;
    }
    
    @Override
    public void guardarDatos(){
        Date date = new Date();
        String formato = "dd/MM/yyyy";  //formato de fecha deseado
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formato);
        String fecha = simpleDateFormat.format(date);
        String linea= String.join(",", this.getID(), this.getUsuario(), "Corriente", this.getNumero(), this.getEstado(), this.getIdChequera(), "---", fecha.toString());              
        ManejoArchivos.escribir("cuentas.txt", linea);
    }
    
    @Override
    public void consultarEstado() {         
        Date date = new Date();
        String formato = "dd/MM/yyyy";  //formato de fecha deseado
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formato);
        String fechaActual = simpleDateFormat.format(date);
        System.out.println("Su cuenta corriente con número:"+ this.getNumero() +" tiene estado\n" +
                    this.getEstado() + " a la fecha: "+ fechaActual);        
    }
}
