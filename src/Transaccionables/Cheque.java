package Transaccionables;
public class Cheque {
    private int codigo;
    private String estado;
    private double monto;
    
    Cheque(int codigo){
        this.codigo = codigo;
        estado = "No usado";
        monto = 0;
    }
    
    public int getCodigo(){
        return codigo;
    }
    
    public String getEstado(){
        return estado;
    }
    
    public void setEstado(String estado){
        this.estado = estado;
    }
    
    public double getMonto(){
        return monto;
    }
    
    public void setMonto(double monto){
        this.monto = monto;
    }
}
