package Transaccionables;

import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class Cuenta implements Transaccionable{
    private String usuario;
    //private String tipo;
    private String numero;
    private String estado;
    private double saldo;
    private String id;
    
    public Cuenta(String usuario, String numero){
        this. usuario = usuario;
        //this.tipo =  tipo;
        this.numero = numero;
        estado = "Activa";
        this.generarID();
        saldo = 300;
    }
    
    public String getNumero(){
        return this.numero;
    }
    
    private void generarID(){
        String id = "";
        for(int i = 0; i < 4; i++){                            
            int entero = (int) Math.floor(Math.random()*9);
            id += entero;
        }
        this.id = id;
    }
    
    public String getID(){
        return id;
    }

    public String getUsuario() {
        return usuario;
    }

    /*public String getTipo() {
        return tipo;
    }*/
    
    public void setEstado(String e){
        estado = e;
    }

    public String getEstado() {
        return estado;
    }
    
    public double getSaldo(){
        return saldo;
    }
    
    public void retirar(double monto){
        saldo -= monto;
    }
    
    public void depositar(double deposito){
        saldo += deposito;
    }
    
    /*@Override
    public void consultarEstado() {         
        Date date = new Date();
        String formato = "dd/MM/yyyy";  //formato de fecha deseado
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formato);
        String fechaActual = simpleDateFormat.format(date);
        System.out.println("Su cuenta con número:"+ numero +" tiene estado\n" +
                    estado + " a la fecha: "+ fechaActual);        
    }*/
    
    public abstract void guardarDatos();   
}
