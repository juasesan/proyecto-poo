package Transaccionables;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Tarjeta implements Transaccionable {
    private String numero;
    private String estado;

    public Tarjeta(String numero) {
        this.numero = numero;
        this.estado = "Pendiente";
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public void consultarEstado(){
        Date date = new Date();
        String formato = "dd/MM/yyyy";  //formato de fecha deseado
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formato);
        String fecha = simpleDateFormat.format(date);
        
        System.out.println("Su tarjeta de debito con numero: " + numero + " tiene estado " + estado + " a la fecha " + fecha);
    }
}
